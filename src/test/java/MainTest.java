import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MainTest  {
    WebDriver driver;
@BeforeClass
public void setup(){
    System.setProperty("webdriver.chrome.driver", "D:\\Monisha\\Edumate-Automation\\chromedriver.exe");
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    driver.navigate().to("https://jupiter.edumate.net/centest/web/app.php/login/");
}


@Test
    public void testFilter() {
    CommonFunction basic=new CommonFunction(driver);

    basic.enter(By.cssSelector("input[name='_username']"),"admin",10);
    basic.enter(By.cssSelector("input[name='_password']"),"edumat3",10);
    basic.click(By.xpath("//span[contains(text(),'Login')]"),10);
    basic.click(By.xpath("//span[text()='Classic']"),10);
    basic.click(By.xpath("//b[contains(text(),'Learning')]"),10);
    basic.click(By.xpath("//a/b[text()='Learning Tasks']"),10);
    basic.staticWait(3000);
    WebElement element = driver.findElement(By.xpath("//iframe[@class='x-component main-content-iframe x-box-item x-component-default']"));
    driver.switchTo().frame(element);
    basic.click(By.xpath("//button[@type='button' and contains(@class,'warning--text')]"),30);
    if((basic.findElementVisibility(By.xpath("//label[text()='Classwork']"),10)!=null)&&(basic.findElementVisibility(By.xpath("//label[text()='Coursework']"),10)!=null)){
      System.out.println("Pass:Element is present");
    }
    else
    {
        System.out.println("Fail:Element is not present");
    }

    }
@AfterClass
    public void quit(){
    driver.quit();
}
}
