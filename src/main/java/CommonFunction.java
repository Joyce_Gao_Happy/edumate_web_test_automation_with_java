
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class CommonFunction {
    private WebDriver driver;

     public CommonFunction(WebDriver driver){
         this.driver=driver;
     }

    public void click(By by,int time) {
        WebElement element = findElementVisibility(by, time);
        staticWait(1000);
        if (element != null) {
            element.click();

        }
    }
    public void enter(By by, String value, int time){
        WebElement element=findElementVisibility(by,time);
        staticWait(500);
        if(element!=null){
            element.clear();
            element.sendKeys(value);

        }
    }
    public WebElement findElementVisibility(final By by, int time) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        try {
            return wait.until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (Exception e) {
            System.out.println();
            return null;
        }

    }
    public void staticWait(int time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
